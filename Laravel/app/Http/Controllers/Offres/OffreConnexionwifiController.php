<?php


namespace App\Http\Controllers\Offres;
use App\Models\ConnexionWifi\ConnexionwifiVue;
use App\Models\ConnexionWifi\ConnexionWifi;
use App\Models\Offrant;
use Carbon\Carbon;
use App\Models\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OffreConnexionwifiController extends Controller
{
    public function index()//Affichage Offres Connexion_wifi
    {
        $connexionwifis = ConnexionwifiVue::all();
        return view('pages.connexionwifi2', compact('connexionwifis'));
    }
    public function store(Request $request)//Enregistrer Offres Connexion_wifi
    {
        Offrant::firstOrCreate(['IDPER' => auth()->user()->getAuthIdentifier()]);
        Service::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'MONTANTSER' =>$request->montant_partage,
            'DATEOFFRESER' =>Carbon::now(),
            'DATEDEBUTSER' =>$request->datedebut_partage,
            'DATEFINSER' =>$request->datefin_partage,
            'BOOL_SERVICECOMPLET' =>0,
        ]);
        $id_service = DB::getPdo()->lastInsertId();
        ConnexionWifi::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'IDSER' =>$id_service,
            'rueWif' => $request->adresse_partage,
            'CPWif' => $request->cp_partage,
            'villeWif' => $request->ville_partage,
        ]);
        return 'Votre offre à été créée';
        return redirect()->back();
    }

}
