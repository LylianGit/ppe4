<?php


namespace App\Http\Controllers\Offres;
use App\Models\Cours\Competence;
use App\Models\Cours\Cours;
use App\Models\Offrant;
use App\Models\Service;
use Carbon\Carbon;
use App\Models\Cours\CoursVue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class OffreCoursController extends Controller
{
    public function index()//affichage Offres Cours
    {
        $cours = CoursVue::all();
        return view('pages.cours2', compact('cours'));
    }
    public function store(Request $request)//Enregistrer Offres Cours
    {
        Offrant::firstOrCreate(['IDPER' => auth()->user()->getAuthIdentifier()]);
        Service::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'MONTANTSER' =>$request->montant_cours,
            'DATEOFFRESER' =>Carbon::now(),
            'DATEDEBUTSER' =>$request->datedebut_cours,
            'DATEFINSER' =>$request->datefin_cours,
            'BOOL_SERVICECOMPLET' =>0,
        ]);
        $id_service = DB::getPdo()->lastInsertId();
        Cours::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'IDSER' => $id_service,
            'villeCou' => $request->ville_cours,
            'rueCou'=> $request->adresse_cours,
            'CPCou' => $request->cp_cours,
        ]);
        Competence::create([
            'LIBCOMPETENCE'=>$request->intitule_cours,
        ]);
        return Redirect::back()->withErrors(['msg', 'Votre offre a bien été créée']);
    }
}
