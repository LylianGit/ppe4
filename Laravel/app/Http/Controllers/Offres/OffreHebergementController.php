<?php


namespace App\Http\Controllers\Offres;
use App\Models\Hebergement\HebergementVue;
use App\Models\Offrant;
use Illuminate\Http\Request;
use App\Models\Hebergement\Hebergement;
use Carbon\Carbon;
use App\Models\Service;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OffreHebergementController extends Controller
{
    public function index()//Affichage Offres Hebergement
{
    $hebergements = HebergementVue::all();

    return view('pages.hebergement2', compact('hebergements'));
}

    public function store(Request $request)//Enregistrer Offres Hebergement
    {
        Offrant::firstOrCreate(['IDPER' => auth()->user()->getAuthIdentifier()]);
        Service::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'MONTANTSER' =>$request->montant_heb,
            'DATEOFFRESER' =>Carbon::now(),
            'DATEDEBUTSER' =>$request->datedeb_heb,
            'DATEFINSER' =>$request->datefin_heb,
            'BOOL_SERVICECOMPLET' =>0,
        ]);
        $id_service = DB::getPdo()->lastInsertId();
        Hebergement::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'IDSER' => $id_service,
            'villeHeb' => $request->ville_heb,
            'rueHeb'=> $request->adresse_heb,
            'CPHeb' => $request->cp_heb,
    ]);
        return 'Votre offre à été créée';
        return redirect()->back();
    }
}
