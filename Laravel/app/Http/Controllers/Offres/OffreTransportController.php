<?php


namespace App\Http\Controllers\Offres;
use App\Models\Postule;
use App\Models\Transport\TransportVue;
use App\Models\Lieu;
use App\Models\Offrant;
use App\Models\Service;
use App\Models\Transport\Transport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OffreTransportController extends Controller
{
    public function index()//Affichage Offres Transport
    {
        $transports = TransportVue::all();
        return view('pages.transport2', compact('transports'));
    }
    public function store(Request $request)//Enregistrer Offres Transport
    {
        Offrant::firstOrCreate(['IDPER' => auth()->user()->getAuthIdentifier()]);
         Lieu::create([
             'VILLELIE'=> $request->ville_depart,
            'ADRLIE' => $request->adresse_depart,
        ]);
        $id_lieu_depart = DB::getPdo()->lastInsertId();
        Lieu::create([
            'VILLELIE'=> $request->ville_arrive,
            'ADRLIE' => $request->adresse_arrive,
        ]);
        $id_lieu_arrive = DB::getPdo()->lastInsertId();
        Service::create([
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'MONTANTSER' =>$request->montant,
            'DATEOFFRESER' =>Carbon::now(),
            'DATEDEBUTSER' =>$request->datedebut,
            'DATEDEFINSER' =>$request->datedebut,
            'BOOL_SERVICECOMPLET' =>0,
        ]);
        $id_service = DB::getPdo()->lastInsertId();
        Transport::create([
            'IDPER'=> auth()->user()->getAuthIdentifier(),
            'IDSER'=>$id_service,
            'IDLIE'=>$id_lieu_depart,
            'IDLIE_ARRIVER'=>$id_lieu_arrive,
            'HEURERDVTRA'=> $request->datedebut,
            'NBPLACESTRA'=> $request->nbplace,
            ]);
        return 'Votre offre à été créée';
        return redirect()->back();
    }
}
