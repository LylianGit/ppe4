<?php


namespace App\Http\Controllers\Offres;


use App\Http\Controllers\Controller;
use App\Models\Postule;
use App\Models\Service;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

       public function index()//Affichage Offres Connexion_wifi
    {
        $notif = DB::table('postule')->count()->where('IDPER_1',auth()->user()->getAuthIdentifier());
    }

    public function store(Request $request)//Enregistrer Offres Transport
    {

        Postule::create([
            'IDPER_1' => $request->reservecoIDPER,
            'IDPER' => auth()->user()->getAuthIdentifier(),
            'IDSER' => $request->reservecoID,
            'BOOL_OBTENTIONSERVICE' => 1,
        ]);
        return redirect()->back()->with('message', 'Réservation effectuée');
    }
    public function update(Request $request)
    {
        $service = Service::all();
        $service->BOOL_SERVICECOMPLET = 1;
    }//Enregistrer Offres Transport
}
