<?php


namespace App\Http\Controllers\Offres;


use App\Http\Controllers\Controller;
use App\Models\VosOffresVue;
use Illuminate\Support\Facades\DB;

class VosOffresController extends Controller
{
    public function index()//Affichage Offres Transport
    {

        $offres1 = DB::table('offre_transport')->where('IDPER', '=',auth()->user()->getAuthIdentifier());
        $offres1=$offres1->get();
        $offres2 = DB::table('offre_connexionwifi')->where('IDPER', '=', auth()->user()->getAuthIdentifier());
        $offres2= $offres2->get();
        $offres3 = DB::table('offre_hebergement')->where('IDPER', '=', auth()->user()->getAuthIdentifier());
        $offres3= $offres3->get();
        $offres4 = DB::table('offre_cours')->where('IDPER', '=', auth()->user()->getAuthIdentifier());
        $offres4= $offres4->get();
        return view('pages.VosOffres', compact('offres1','offres2','offres3','offres4'));
    }
}
