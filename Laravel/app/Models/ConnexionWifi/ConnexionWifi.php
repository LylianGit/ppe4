<?php

namespace App\Models\ConnexionWifi;

use Illuminate\Database\Eloquent\Model;

class ConnexionWifi extends Model
{
    protected $table = 'wifi';
    protected $fillable=['IDPER','IDSER','rueWif','CPWif','villeWif'];
    public $timestamps = false;

    public function Connexionwifi()
    {
        return $this->morphOne('App\Models\Service','transportable');
    }

}
