<?php

namespace App\Models\ConnexionWifi;

use Illuminate\Database\Eloquent\Model;

class ConnexionwifiVue extends Model
{
    protected $table = 'offre_connexionwifi';
    protected $fillable=['IDPER','IDSER','IDLIE','IDLIE_ARRIVER','HEURERDVTRA','NBPLACESTRA'];
    public $timestamps = false;

    public function lieu()
    {
        return $this->hasMany('App\Models\Lieu','IDLIE','IDLIE');
    }
    public function service()
    {
        return $this->morphMany('App\Models\Service','transportable');
    }

}
