<?php


namespace App\Models\Cours;


use Illuminate\Database\Eloquent\Model;

class Competence extends Model
{
    protected $table='competence';
    protected $fillable=['IDCOMPETENCE','LIBCOMPETENCE'];
    public $timestamps = false;

    public function coursable()
    {
        return $this->belongsTo('App\Models\Cours');
    }
}
