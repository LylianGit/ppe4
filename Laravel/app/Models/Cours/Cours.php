<?php


namespace App\Models\Cours;
use Illuminate\Database\Eloquent\Model;

class Cours extends Model
{
    protected $table='cours';
    protected $fillable=['IDPER','IDSER','rueCou','CPCou','villeCou'];
    public $timestamps = false;

    public function Cours()
    {
        return $this->morphOne('App\Models\Service','transportable');
    }
    public function competence()
    {
        return $this->hasMany('App\Models\Competence','IDCOMPETENCE');
    }
}
