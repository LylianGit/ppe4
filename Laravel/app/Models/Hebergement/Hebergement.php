<?php


namespace App\Models\Hebergement;


use Illuminate\Database\Eloquent\Model;

class Hebergement extends Model
{
    protected $table='hebergement';
    protected $fillable=['IDPER','IDSER','rueHeb','CPHeb','villeHeb'];
    public $timestamps = false;

    public function hebergement()
    {
        return $this->morphOne('App\Models\Service','transportable');
    }
}
