<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Lieu extends Model
{

    protected $table='lieu';
    protected $fillable=['VILLELIE','ADRLIE','IDLIE'];
    public $timestamps = false;

    public function transport()
    {
        return $this->belongsTo('App\Models\Transport\Transport');
    }

}
