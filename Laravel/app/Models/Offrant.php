<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Offrant extends Model
{
    protected $table='offrant';
    protected $fillable=['IDPER','PHOTOETU'];
    public $timestamps = false;

    public function transportable()
    {
        return $this->belongsTo('App\Models\Service');
    }
}
