<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Postule extends Model
{
    protected $primaryKey = 'IDSER';
    protected $table='postule';
    protected $fillable=['IDPER_1','IDSER','IDPER','BOOL_OBTENTIONSERVICE','COMMENTAIRE'];
    public $timestamps = false;
}
