<?php


namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    const CREATED_AT = 'DATEOFFRESER';
    protected $table='service';
    protected $fillable=['IDPER','IDSER','LIBSER','MONTANTSER','DATEOFFRESER','DATEDEBUTSER','DATEFINSER','BOOL_SERVICECOMPLET'];
    public $timestamps = false;

    public function transportable()
    {
        return $this->morphTo('transportable');
    }
    public function offrant()
    {
        return $this->hasOne('App\Models\Offrant');
    }
}
