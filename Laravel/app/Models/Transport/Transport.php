<?php


namespace App\Models\Transport;
use App\Models\Service;


class Transport extends Service
{
    protected $table='transport';
    protected $fillable=['IDPER','IDSER','IDLIE','IDLIE_ARRIVER','HEURERDVTRA','NBPLACESTRA'];
    public $timestamps = false;

    public function lieu()
    {
        return $this->hasMany('App\Models\Lieu','IDLIE','IDLIE');
    }
    public function service()
    {
       return $this->morphOne('App\Models\Service','transportable');
    }

}
