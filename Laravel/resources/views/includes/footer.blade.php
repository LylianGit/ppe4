<!-- Footer Links -->
        <div class="container text-center text-md-left">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->


                <!-- Grid column -->
                <div class="col-md-6 mx-auto">

                    <!-- Links -->
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Nous contacter</h5>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#!"><i class="fas fa-phone"></i> 06-02-45-62-07</a>
                        </li>
                        <li>
                            <a href="#!"><i class="fas fa-envelope-open-text"></i> cas-maraderie@gmail.com</a>
                        </li>
                        <li>
                            <i class="fas fa-map-marker-alt"></i><a class="adr-f" href="https://www.google.com/maps/place/Lyc%C3%A9e+G%C3%A9n%C3%A9ral+%26+Professionnel+Chevrollier/@47.454789,-0.5611381,17z/data=!3m1!4b1!4m5!3m4!1s0x480878a40bf720bf:0xbdbd66228a037a2d!8m2!3d47.454789!4d-0.5589494" onclick="window.open(this.href); return false;"> 2 Rue Adrien Recouvreur 49100 ANGERS France</a>
                        </li>
                    </ul>

                </div>
                <!-- Grid column -->

                <hr class="clearfix w-100 d-md-none">

                <!-- Grid column -->
                <div class="col-md-6 mx-auto">

                    <!-- Links -->
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Plus</h5>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#!">CGU, CGA et CGV</a>
                        </li>
                        <li>
                            <a href="#!">Gestion des données personnelles</a>
                        </li>
                        <li>
                            <a href="#!">Mentions légales</a>
                        </li>
                    </ul>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

        <hr>

        <!-- Social buttons -->
        <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item">
                <a class="">
                    <i class="fab fa-facebook-f fa-2x"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="">
                    <i class="fab fa-twitter fa-2x"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="">
                    <i class="fab fa-google-plus-g fa-2x"> </i>
                </a>
            </li>
            <li class="list-inline-item">
                <a class="">
                    <i class="fab fa-linkedin-in fa-2x"> </i>
                </a>
            </li>
        </ul>
        <!-- Social buttons -->
        <hr>
        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
            Maraderie. Tous droits réservés.
        </div>
        <!-- Copyright -->

