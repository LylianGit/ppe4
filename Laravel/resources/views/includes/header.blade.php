<nav class="navbar navbar-dark navbar-expand-lg">
    <a class="navbar-brand" href="{{ ('/') }}"><img src="Image/logo_entraide.png" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ ('/transport') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Transport
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ ('/transport') }}">Postuler</a>
                    <a class="dropdown-item" href="{{ ('/transport2') }}">Voir les offres</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ ('/connexionwifi') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Connexion Wifi
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ ('/connexionwifi') }}">Postuler</a>
                    <a class="dropdown-item" href="{{ ('/connexionwifi2') }}">Voir les offres</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ ('/hebergement') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Hébergement
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ ('/hebergement') }}">Postuler</a>
                    <a class="dropdown-item" href="{{ ('/hebergement2') }}">Voir les offres</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="{{ ('/cours') }}" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cours
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ ('/cours') }}">Postuler</a>
                    <a class="dropdown-item" href="{{ ('/cours2') }}">Voir les offres</a>
                </div>
            </li>
        </ul>
    </div>
    <ul class="navbar-nav ml-auto">
                            @guest
                                <li class="nav-item"><a href="{{ route('login') }}" class="button btn btn-primary">{{ __('Connexion') }}</a></li><span class="espace"></span>
                                @if (Route::has('register'))
                                    <li class="nav-item"><a href="{{ route('register') }}" class="button btn btn-secondary">{{ __('Inscription') }} </a></li>
                                @endif
                            @else
                             @if(auth()->check())
                              <li class="nav-item">
                                 <a class="nav-link" href="{{ ('/VosOffres') }}">
                                     <i class="fas fa-bell"></i> Vos Offres</a>
                                 </li>
                              @endif
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
            </ul>
</nav>
