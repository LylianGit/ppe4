<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    @include('includes.head')
</head>
<body>
<header>
    @include('includes.header')
</header>

<div id="main">
    @yield('content')
</div>

</body>
<footer>
    @include('includes.footer')
</footer>
</html>
