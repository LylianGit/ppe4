@extends('layouts.default')

@section('content')
    <h1>Vos Offres</h1>


        @foreach($offres2 as $o2)<!-- connexion_wifi -->
        <div class="card">
        <h5 class="card-header">Offre numéro {{ $o2->IDSER }}</h5>
        <div class="card-body">
                <h5 class="card-title">Information sur l'offre de connexion wifi</h5>
                <ul>
                    <li><span class="text-info font-weight-bold">Intitulé : </span>{{ $o2->LIBSER }}</li>
                    <li><span class="text-info font-weight-bold">Date et heure de début : </span>{{ $o2->DATEDEBUTSER }}</li>
                    <li><span class="text-info font-weight-bold">Date et heure de fin : </span>{{ $o2->DATEFINSER }}</li>
                    <li><span class="text-info font-weight-bold">Adresse wifi : </span>{{ $o2->rueWif }}, {{ $o2->CPWif }} {{ $o2->villeWif }}</li>
                    <li><span class="text-info font-weight-bold">Montant : </span>{{ $o2->MONTANTSER }} euros</li>
                </ul>
        </div>
        </div>
        <form class="form-inline space" method="post" action="{{ route('reservation.store') }}">
            @csrf
            <input type="hidden" name="reservecoIDPER" value="{{ $o2->IDPER}}"/>
            <input type="hidden" name="reservecoID" value="{{ $o2->IDSER }}"/>
            <button class="btn btn-success" type="submit">Accepter</button>
            <button class="btn btn-danger" type="submit">Refuser</button>
        </form>
        <br>
        @endforeach
                @foreach($offres4 as $o4)<!-- cours -->
                    <div class="card">
                        <h5 class="card-header">Offre numéro {{ $o4->IDSER }}</h5>
                    <div class="card-body">
                            <h5 class="card-title">Information sur l'offre de cours</h5>
                            <ul>
                                <li><span class="text-info font-weight-bold">Intitulé : </span>{{ $o4->LIBSER }}</li>
                                <li><span class="text-info font-weight-bold">Compétence du cours/étudiant : </span>{{ $o4->libCompetence }}</li>
                                <li><span class="text-info font-weight-bold">Date et heure de début : </span>{{ $o4->DATEDEBUTSER }}</li>
                                <li><span class="text-info font-weight-bold">Date et heure de fin : </span>{{ $o4->DATEFINSER }}</li>
                                <li><span class="text-info font-weight-bold">Adresse cours : </span>{{ $o4->rueCou }}, {{ $o4->CPCou }} {{ $o4->villeCou }}</li>
                                <li><span class="text-info font-weight-bold">Montant : </span>{{ $o4->MONTANTSER }} euros</li>
                            </ul>
                    </div>
                </div>
                <form class="form-inline space" method="post" action="{{ route('reservation.store') }}">
                    @csrf
                    <input type="hidden" name="reservecoIDPER" value="{{ $o4->IDPER}}"/>
                    <input type="hidden" name="reservecoID" value="{{ $o4->IDSER }}"/>
                    <button class="btn btn-success" type="submit">Accepter</button>
                    <button class="btn btn-danger" type="submit">Refuser</button>
                </form>
                <br>
                @endforeach
                @foreach($offres1 as $o1)<!-- transport -->
                <div class="card">
                    <h5 class="card-header">Offre numéro {{ $o1->IDSER }}</h5>
                    <div class="card-body">
                            <h5 class="card-title">Information sur l'offre de transport</h5>
                            <ul>
                                <li><span class="text-info font-weight-bold">Intitulé : </span>{{ $o1->LIBSER }}</li>
                                <li><span class="text-info font-weight-bold">Date et heure : </span>{{ $o1->DATEDEBUTSER }}</li>
                                <li><span class="text-info font-weight-bold">Adresse de départ : </span>{{ $o1->adress_depart }}, {{ $o1->ville_depart }}</li>
                                <li><span class="text-info font-weight-bold">Adresse d'arrivée : </span>{{ $o1->adress_arrivee }}, {{ $o1->ville_arrivee }}</li>
                                <li><span class="text-info font-weight-bold">Nombres de places : </span>{{ $o1->NBPLACESTRA }}</li>
                                <li><span class="text-info font-weight-bold">Montant : </span>{{$o1->MONTANTSER }} euros</li>
                            </ul>
                        <form class="form-inline space" method="post" action="{{ route('reservation.store') }}">
                            @csrf
                            <input type="hidden" name="reservecoIDPER" value="{{ $o1->IDPER}}"/>
                            <input type="hidden" name="reservecoID" value="{{ $o1->IDSER }}"/>
                            <button  class="btn btn-success" type="submit">Accepter</button>
                            <button  class="btn btn-danger" type="submit">Refuser</button>
                        </form>
                        <br>
                    </div>

                </div>

            @endforeach
        @foreach($offres3 as $o3)<!-- hebergement -->
        <div class="card">
            <h5 class="card-header">Offre numéro {{ $o3->IDSER }}</h5>
            <div class="card-body">
                <h5 class="card-title">Information sur l'offre d'hébergement</h5>
                <ul>
                    <li><span class="text-info font-weight-bold">Intitulé : </span>{{ $o3->LIBSER }}</li>
                    <li><span class="text-info font-weight-bold">Date et heure de début : </span>{{ $o3->DATEDEBUTSER }}</li>
                    <li><span class="text-info font-weight-bold">Date et heure de fin : </span>{{ $o3->DATEFINSER }}</li>
                    <li><span class="text-info font-weight-bold">Adresse hébergement : </span>{{ $o3->rueHeb }} {{ $o3->CPHeb }} {{ $o3->villeHeb }} </li>
                    <li><span class="text-info font-weight-bold">Montant : </span>{{$o3->MONTANTSER }} euros</li>
                </ul>
                <form class="form-inline space" method="post" action="{{ route('reservation.store') }}">
                    @csrf
                    <input type="hidden" name="reservecoIDPER" value="{{ $o3->IDPER}}"/>
                    <input type="hidden" name="reservecoID" value="{{ $o3->IDSER }}"/>
                    <button  class="btn btn-success" type="submit">Accepter</button>
                    <button  class="btn btn-danger" type="submit">Refuser</button>
                </form>
                <br>
            </div>
        </div>
        @endforeach
@endsection

