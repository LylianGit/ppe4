@extends('layouts.default')
@section('content')
        <div class="jumbotron">
            <div class="container">

                <h1 class="display-3">Présentation</h1>

                 <p>Maraderie est une jeune communauté qui souhaite créer des services d'entraides entre étudiants
                <br>Elle propose plusieurs services :<br><br>
                Service de transport/covoiturage<br>
                Service de partage de connexion Wifi<br>
                Service d'hébergement pour les veilles d'examens<br>
                     Service de partage de cours en fonction des compétences</p><br>
                <p>
                    <a class="btn btn-primary btn-lg" href="" role="button">En Savoir plus »</a>
                </p>
            </div>
        </div>
        <!-- carousel -->
        <div id="carousel" class="container-fluid  carousel slide col-md-6 mx-auto" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target=".carousel" data-slide-to="0" class="active"></li>
                <li data-target=".carousel" data-slide-to="1"></li>
                <li data-target=".carousel" data-slide-to="2"></li>
                <li data-target=".carousel" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="Image/carsharing.jpg" alt="covoiturage">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="Image/connexion_wifi.jpg" alt="connexion_wifi">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="Image/cours.jpg" alt="cours">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="Image/hebergement.jpg" alt="hebergement">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carousel" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    <br>
@endsection
