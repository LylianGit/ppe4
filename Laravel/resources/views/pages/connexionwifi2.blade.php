@extends('layouts.default')

@section('content')
    <h1>Les offres de connexion wifi</h1>

        @foreach($connexionwifis as $c)
            <div class="card">
                <h5 class="card-header">Offre numéro {{ $c->IDSER }}</h5>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                    <h5 class="card-title">Information sur l'offre</h5>
                        <ul>
                            <li><span class="text-primary font-weight-bold">Intitulé : </span>{{ $c->LIBSER }}</li>
                            <li><span class="text-primary font-weight-bold">Date et heure de début : </span>{{ $c->DATEDEBUTSER }}</li>
                            <li><span class="text-primary font-weight-bold">Date et heure de fin : </span>{{ $c->DATEFINSER }}</li>
                            <li><span class="text-primary font-weight-bold">Adresse wifi : </span>{{ $c->rueWif }}, {{ $c->CPWif }} {{ $c->villeWif }}</li>
                            <li><span class="text-primary font-weight-bold">Montant : </span>{{ $c->MONTANTSER }} euros</li>
                        </ul>

                    <footer class="blockquote-footer"><cite title="Source Title" style="color:#FFFFFF";>{{ $c->prenom }} {{ $c->name }}</cite></footer>
                    </blockquote><br>
                    <form method="post" action="{{ route('reservation.store') }}">
                        @csrf
                        <input type="hidden" name="reservecoIDPER" value="{{ $c->IDPER}}"/>
                        <input type="hidden" name="reservecoID" value="{{ $c->IDSER }}"/>
                        <button class="btn btn-primary" type="submit">Réserver</button>
                    </form>
                </div>
            </div>
            <br>
        @endforeach
@endsection

