@extends('layouts.default')

@section('content')
    <br>
    <div class="container customdiv rounded">
        <div class="panel panel-info">
            <form method="POST" action="{{ route('cours.store') }}" id="rendered-form"><div class="rendered-form">
                    @csrf
                    <!-- <div class="form-group">
                        <label for="company-content">Intitulé du cours</label>
                        <select name="competence" id="" class="form-control">
                            <option> </option>
                            <option>Homme</option>
                        </select>
                    </div> -->
                    <div class=""><h1 id="control-4015327">Créer votre offre</h1></div>
                    <div class=""><h2 id="control-7218839">Offre de cours</h2></div>
                    <div class="formbuilder-text form-group field-text-1581462060148"><label for="text-1581462060148" class="formbuilder-text-label">Intitulé du cours<span class="formbuilder-required">*</span></label><input type="text" placeholder="Ecrivez l'intitulé du cours (compétence)" class="form-control" name="intitule_cours" access="false" maxlength="50" id="text-1581462060148"></div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Ville du cours<span class="formbuilder-required">*</span></label><input type="text" placeholder="Ecrivez l'adresse du partage " class="form-control" name="ville_cours" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Adresse du cours<span class="formbuilder-required">*</span></label><input type="text" placeholder="Ecrivez l'adresse du partage " class="form-control" name="adresse_cours" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Code postal<span class="formbuilder-required">*</span></label><input type="text" placeholder="Ecrivez l'adresse du partage " class="form-control" name="cp_cours" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    <div class="formbuilder-date form-group field-date-1579787818264"><label for="date-1579787818264" class="formbuilder-date-label">Date et heure du début du cours<span class="formbuilder-required">*</span></label><input type="datetime-local" placeholder="Sélectionnez la date et l'heure prévue du début du partage" class="form-control" name="datedebut_cours" id="date-1579787818264" required="required" aria-required="true"></div>
                    <div class="formbuilder-date form-group field-date-1579795763248"><label for="date-1579795763248" class="formbuilder-date-label">Date et heure de fin du cours<span class="formbuilder-required">*</span></label><input type="datetime-local" placeholder="Sélectionnez la date et l'heure prévue de fin du partage" class="form-control" name="datefin_cours" id="date-1579795763248" required="required" aria-required="true"></div>
                    <div class="formbuilder-number form-group field-number-1579788342545"><label for="number-1579788342545" class="formbuilder-number-label">Montant&nbsp;</label><input type="number" placeholder="Sélectionnez le montant du service" class="form-control" name="montant_cours" value="0" min="0" max="50" step="0.5" id="number-1579788342545"></div>
                    <div class="form-inline">
                        <div class="formbuilder-button form-group field-button-1579788425443"><button type="submit" class="btn-info btn" name="button-1579788425443" style="info" id="button-1579788425443">Confirmez&nbsp;</button></div>
                        <div class="formbuilder-button form-group field-button-1579788495393"><button type="reset" class="btn-danger btn" name="button-1579788495393" style="default" id="button-1579788495393">Annuler</button></div>
                    </div></div></form>
                </div>
        </div>
    <br><br><br>
    <script src="{{ asset('js/form-builder.js') }}" defer></script>
@endsection
