@extends('layouts.default')

@section('content')
    <br>
    <div class="container customdiv rounded">
        <div class="panel panel-info">
            <form method="POST" id="rendered-form" action="{{ route('hebergement.store') }}"><div class="rendered-form">
                    @csrf
                    <div class=""><h1 id="control-4571539">Créer votre offre</h1></div>
                    <div class=""><h2 id="control-2547823">Offre de&nbsp; partage de logement (hébergement)</h2></div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Ville de l'hébergement<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez la ville de l'hébergement" class="form-control" name="ville_heb" maxlength="50" id="text-1579788113961" required="required" aria-required="true">
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Adresse de l'hébergement<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez l'adresse de l'hébergement" class="form-control" name="adresse_heb" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                        <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Code Postal de l'hébergement<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez le code postal de l'hébergement" class="form-control" name="cp_heb" pattern="[0-9]{5}" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    <div class="formbuilder-date form-group field-date-1579787818264"><label for="date-1579787818264" class="formbuilder-date-label">Date et heure du début du partage<span class="formbuilder-required">*</span></label><input type="datetime-local" placeholder="Sélectionnez la date et l'heure prévue du début du partage" class="form-control" name="datedeb_heb" id="date-1579787818264" required="required" aria-required="true"></div>
                    <div class="formbuilder-date form-group field-date-1579795763248"><label for="date-1579795763248" class="formbuilder-date-label">Date et heure de fin du partage<span class="formbuilder-required">*</span></label><input type="datetime-local" placeholder="Sélectionnez la date et l'heure prévue de fin du partage" class="form-control" name="datefin_heb" id="date-1579795763248" required="required" aria-required="true"></div>
                    <div class="formbuilder-checkbox-group form-group field-checkbox-group-1579797385531"><label for="checkbox-group-1579797385531" class="formbuilder-checkbox-group-label"><span style="font-family: Roboto," helvetica,="" neue,="" helvetica,="" arial,="" sans-serif;="" font-size:="" 14px;="" font-style:="" normal;="" font-variant-ligatures:="" font-variant-caps:="" font-weight:="" 400;="">Options</span><span class="formbuilder-required">*</span></label>
                        <div class="checkbox-group"><div class="formbuilder-checkbox-inline"><label for="checkbox-group-1579797385531-0" class="kc-toggle"><input name="checkbox-group-1579797385531[]" id="checkbox-group-1579797385531-0" aria-required="true" value="0" type="checkbox"><span></span>Lit </label></div>
                            <div class="formbuilder-checkbox-inline"><label for="checkbox-group-1579797385531-1" class="kc-toggle"><input name="checkbox-group-1579797385531[]" id="checkbox-group-1579797385531-1" aria-required="true" value="0" type="checkbox" ><span></span>Salle de bain</label></div>
                            <div class="formbuilder-checkbox-inline"><label for="checkbox-group-1579797385531-2" class="kc-toggle"><input name="checkbox-group-1579797385531[]" id="checkbox-group-1579797385531-2" aria-required="true" value="0" type="checkbox" ><span></span>Accès internet</label></div>
                            <div class="formbuilder-checkbox-inline"><label for="checkbox-group-1579797385531-3" class="kc-toggle"><input name="checkbox-group-1579797385531[]" id="checkbox-group-1579797385531-3" aria-required="true" value="0" type="checkbox" ><span></span>Coin cuisine</label></div>
                            <div class="formbuilder-checkbox-inline"><input name="checkbox-group-1579797385531[]" id="checkbox-group-1579797385531-other" aria-required="true" class="undefined other-option" value="" type="checkbox"><label for="checkbox-group-1579797385531-other">Autres<input type="text" id="checkbox-group-1579797385531-other-value" class="other-val"></label></div></div></div></div>
                    <div class="formbuilder-number form-group field-number-1579788342545"><label for="number-1579788342545" class="formbuilder-number-label">Montant&nbsp;</label><input type="number" placeholder="Sélectionnez le montant du service" class="form-control" name="montant_heb" value="0" min="0" max="50" step="0.5" id="number-1579788342545"></div>
                        <div class="form-inline space">
                            <div class="formbuilder-button form-group field-button-1579788425443"><button type="submit" class="btn-info btn" name="button-1579788425443" style="info" id="button-1579788425443">Confirmez&nbsp;</button></div>
                            <div class="formbuilder-button form-group field-button-1579788495393"><button type="reset" class="btn-danger btn" name="button-1579788495393" style="default" id="button-1579788495393">Annuler</button></div></div></div></form>
        </div>
    </div>
<br><br><br>
        <script src="{{ asset('js/form-builder.js') }}" defer></script>
@endsection
