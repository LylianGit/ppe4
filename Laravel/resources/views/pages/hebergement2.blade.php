@extends('layouts.default')

@section('content')
    <h1>Les offres d'hebergement</h1>

        @foreach($hebergements as $h)
            <div class="card">
                <h5 class="card-header">Offre numéro {{ $h->IDSER }}</h5>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                    <h5 class="card-title">Information sur l'offre</h5>
                        <ul>
                            <li><span class="text-primary font-weight-bold">Intitulé : </span>{{ $h->LIBSER }}</li>
                            <li><span class="text-primary font-weight-bold">Date et heure de début : </span>{{ $h->DATEDEBUTSER }}</li>
                            <li><span class="text-primary font-weight-bold">Date et heure de fin : </span>{{ $h->DATEFINSER }}</li>
                            <li><span class="text-primary font-weight-bold">Adresse hébergement : </span>{{ $h->rueHeb }}, {{ $h->CPHeb }} {{ $h->villeHeb }}</li>
                            <li><span class="text-primary font-weight-bold">Montant : </span>{{ $h->MONTANTSER }} euros</li>
                        </ul>
                    <footer class="blockquote-footer"><cite title="Source Title" style="color:#FFFFFF";>{{ $h->prenom }} {{ $h->name }}</cite></footer>
                    </blockquote><br>
                    <a href="#" class="btn btn-primary">Réserver</a>
                </div>
            </div>
            <br>
        @endforeach
@endsection

