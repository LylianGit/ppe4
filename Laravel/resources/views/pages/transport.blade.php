@extends('layouts.default')

@section('content')
    <br>

    <div class="container customdiv rounded">
        <div class="panel panel-info">
            <form method="POST" action="{{ route('transport.store') }}" id="rendered-form"><div class="rendered-form">
                    @csrf
                    <div class=""><h1 id="control-4015327">Créer votre offre</h1></div>
                    <div class=""><h2 id="control-7218839">Offre de transport (co-voiturage)</h2></div>
                    <br>
                    <div class="customdiv2 rounded">
                        <div class="custom-top rounded-top btn-info">
                        Renseignez Le lieu de départ
                            <hr class="hrdark">
                        </div>
                    <div class="formbuilder-text form-group field-text-1579787996740"><label for="text-1579787996740" class="formbuilder-text-label">Ville de départ<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez la de ville de départ" class="form-control" name="ville_depart" maxlength="50" id="text-1579787996740" required="required" aria-required="true" value=""></div>
                    <div class="formbuilder-text form-group field-text-1579787996740"><label for="text-1579787996740" class="formbuilder-text-label">Adresse de départ<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez l'adresse de départ " class="form-control" name="adresse_depart" maxlength="50" id="text-1579787996740" required="required" aria-required="true"></div>
                    </div>
                    <br>
                    <div class="customdiv2 rounded ">
                        <div class="custom-top rounded-top btn-info">
                        Renseignez Le lieu d'arrivé
                            <hr class="hrdark">
                        </div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Ville D'arrivée<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez la ville d'arrivée" class="form-control" name="ville_arrive" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    <div class="formbuilder-text form-group field-text-1579788113961"><label for="text-1579788113961" class="formbuilder-text-label">Adresse D'arrivée<span class="formbuilder-required">*</span></label><input type="text" placeholder="Insérez l'adresse d'arrivée" class="form-control" name="adresse_arrive" maxlength="50" id="text-1579788113961" required="required" aria-required="true"></div>
                    </div>
                    <div class="formbuilder-date form-group field-date-1579787818264"><label for="date-1579787818264" class="formbuilder-date-label">Date et heure de rendez-vous<span class="formbuilder-required">*</span></label><input type="datetime-local" placeholder="Sélectionnez la date et l'heure prévue du rendez-vous" class="form-control" name="datedebut" id="date-1579787818264" required="required" aria-required="true"></div>
                    <div class="formbuilder-number form-group field-number-1579787367864"><label for="number-1579787367864" class="formbuilder-number-label">Nombre de place<span class="formbuilder-required">*</span></label><input type="number" placeholder="Sélectionnez le nombre de place disponible" class="form-control" name="nbplace" value="1" min="1" max="7" step="1" id="number-1579787367864" required="required" aria-required="true"></div>
                    <div class="formbuilder-number form-group field-number-1579788342545"><label for="number-1579788342545" class="formbuilder-number-label">Montant&nbsp;</label><input type="number" placeholder="Sélectionnez le montant du service" class="form-control" name="montant" value="0" min="0" max="50" step="0.5" id="number-1579788342545"></div>
                    <div class="form-inline space">
                    <div class="formbuilder-button form-group field-button-1579788425443"><button type="submit" class="btn-info btn" name="button-1579788425443" style="info" id="button-1579788425443">Confirmez&nbsp;</button></div>
                    <div class="formbuilder-button form-group field-button-1579788495393"><button type="reset" class="btn-danger btn" name="button-1579788495393" style="default" id="button-1579788495393">Annuler</button></div>
                    </div>
                </div></form>
        </div>
    </div>
<br><br><br>
    <script src="{{ asset('js/form-builder.js') }}" defer></script>
@endsection

