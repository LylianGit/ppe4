@extends('layouts.default')

@section('content')
    <h1>Les offres de transport</h1>

        @foreach($transports as $t)
            <div class="card">
                <h5 class="card-header">Offre numéro {{ $t->IDSER }}</h5>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                    <h5 class="card-title">Information sur l'offre</h5>
                        <ul>
                            <li><span class="text-primary font-weight-bold">Intitulé : </span>{{ $t->LIBSER }}</li>
                            <li><span class="text-primary font-weight-bold">Date et heure : </span>{{ $t->DATEDEBUTSER }}</li>
                            <li><span class="text-primary font-weight-bold">Adresse de départ : </span>{{ $t->adress_depart }}, {{ $t->ville_depart }}</li>
                            <li><span class="text-primary font-weight-bold">Adresse d'arrivée : </span>{{ $t->adress_arrivee }}, {{ $t->ville_arrivee }}</li>
                            <li><span class="text-primary font-weight-bold">Nombres de places : </span>{{ $t->NBPLACESTRA }}</li>
                            <li><span class="text-primary font-weight-bold">Montant : </span>{{ $t->MONTANTSER }} euros</li>
                        </ul>
                    <footer class="blockquote-footer"><cite title="Source Title" style="color:#FFFFFF";>{{ $t->prenom }} {{ $t->name }}</cite></footer>
                    </blockquote><br>
                    <form method="post" action="{{ route('reservation.store') }}">
                        @csrf
                        <input type="hidden" name="reservecoIDPER" value="{{ $t->IDPER}}"/>
                        <input type="hidden" name="reservecoID" value="{{ $t->IDSER }}"/>
                        <button class="btn btn-primary" type="submit">Réserver</button>
                    </form>
                </div>
            </div>
            <br>
        @endforeach
@endsection

