<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//accès publique
Route::get('/', function()
{
    return View::make('pages.accueil');
});
//accès autorisé que si l'utilisateur est connecté
Route::get('transport', function () {
    if(auth()->check()) { return View::make('pages.transport');}
    else{return redirect('/login');};
});
Route::get('connexionwifi', function () {
    if(auth()->check()) { return View::make('pages.connexionwifi');}
    else{return redirect('/login');};
});
Route::get('hebergement', function () {
    if(auth()->check()) {  return View::make('pages.hebergement');}
    else{return redirect('/login');};
});
Route::get('cours', function () {
    if(auth()->check()) {return View::make('pages.cours');}
    else{return redirect('/login');};
});

//Route pour afficher les Offres
Route::get('/transport2', 'Offres\OffreTransportController@index');//Transport
Route::get('/hebergement2', 'Offres\OffreHebergementController@index');//Hebergement
Route::get('/cours2', 'Offres\OffreCoursController@index');//Cours
Route::get('/connexionwifi2', 'Offres\OffreConnexionwifiController@index');//Connexion Wifi
Route::get('/VosOffres', 'Offres\VosOffresController@index');

//Route pour récupérer les Offres
Route::post('/transport2', 'Offres\OffreTransportController@store')->name('transport.store');//Transport
Route::post('/hebergement2', 'Offres\OffreHebergementController@store')->name('hebergement.store');//Hebergement
Route::post('/cours2', 'Offres\OffreCoursController@store')->name('cours.store');//Cours
Route::post('/connexionwifi2', 'Offres\OffreConnexionwifiController@store')->name('connexion_wifi.store');//Connexion Wifi
Route::post('/', 'Offres\ReservationController@store')->name('reservation.store');

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('/home');
