﻿namespace casmaraderie
{
    partial class TousLesUtilisateurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGVPR = new System.Windows.Forms.DataGridView();
            this.cbEtu = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnSupprimer = new System.Windows.Forms.Button();
            this.BtnModifier = new System.Windows.Forms.Button();
            this.BtnAjouter = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPR)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGVPR
            // 
            this.dataGVPR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGVPR.Location = new System.Drawing.Point(58, 59);
            this.dataGVPR.Name = "dataGVPR";
            this.dataGVPR.Size = new System.Drawing.Size(707, 335);
            this.dataGVPR.TabIndex = 0;
            // 
            // cbEtu
            // 
            this.cbEtu.FormattingEnabled = true;
            this.cbEtu.Location = new System.Drawing.Point(338, 21);
            this.cbEtu.Name = "cbEtu";
            this.cbEtu.Size = new System.Drawing.Size(190, 21);
            this.cbEtu.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(286, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tri :";
            // 
            // BtnSupprimer
            // 
            this.BtnSupprimer.Location = new System.Drawing.Point(531, 415);
            this.BtnSupprimer.Name = "BtnSupprimer";
            this.BtnSupprimer.Size = new System.Drawing.Size(84, 23);
            this.BtnSupprimer.TabIndex = 11;
            this.BtnSupprimer.Text = "SUPPRIMER";
            this.BtnSupprimer.UseVisualStyleBackColor = true;
            // 
            // BtnModifier
            // 
            this.BtnModifier.Location = new System.Drawing.Point(370, 415);
            this.BtnModifier.Name = "BtnModifier";
            this.BtnModifier.Size = new System.Drawing.Size(75, 23);
            this.BtnModifier.TabIndex = 10;
            this.BtnModifier.Text = "MODIFIER";
            this.BtnModifier.UseVisualStyleBackColor = true;
            // 
            // BtnAjouter
            // 
            this.BtnAjouter.Location = new System.Drawing.Point(217, 415);
            this.BtnAjouter.Name = "BtnAjouter";
            this.BtnAjouter.Size = new System.Drawing.Size(75, 23);
            this.BtnAjouter.TabIndex = 9;
            this.BtnAjouter.Text = "AJOUTER";
            this.BtnAjouter.UseVisualStyleBackColor = true;
            this.BtnAjouter.Click += new System.EventHandler(this.BtnAjouter_Click);
            // 
            // ToutLesUtilisateurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 469);
            this.Controls.Add(this.BtnSupprimer);
            this.Controls.Add(this.BtnModifier);
            this.Controls.Add(this.BtnAjouter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbEtu);
            this.Controls.Add(this.dataGVPR);
            this.Name = "ToutLesUtilisateurs";
            this.Text = "TousLesUtilisateurs";
            this.Load += new System.EventHandler(this.TousLesUtilisateurs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGVPR)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGVPR;
        private System.Windows.Forms.ComboBox cbEtu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnSupprimer;
        private System.Windows.Forms.Button BtnModifier;
        private System.Windows.Forms.Button BtnAjouter;
    }
}