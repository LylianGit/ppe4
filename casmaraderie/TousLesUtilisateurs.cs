﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace casmaraderie
{
    public partial class TousLesUtilisateurs : Form
    {

        private int nb = 1;
        private BindingSource bS1;
        public TousLesUtilisateurs()
        {
            InitializeComponent();
        }

        private void TousLesUtilisateurs_Load(object sender, EventArgs e)
        {
            controleur.init();
            controleur.Vmodele.seconnecter();
            if (controleur.Vmodele.Connopen == false)
            {
                MessageBox.Show("Erreur dans la connexion");
            }
            else
            {
                if (nb != 1)
                    MessageBox.Show("BD connectée", "Information BD", MessageBoxButtons.OK, MessageBoxIcon.Information);

                controleur.Vmodele.charger_donnees("etudiant");      // chargement des données de la table ETUDIANT
                if (controleur.Vmodele.Chargement)
                {
                    // un DT par table
                    bS1 = new BindingSource();

                    bS1.DataSource = controleur.Vmodele.DT[1];
                    dataGVPR.DataSource = bS1;
                    dataGVPR.Columns[0].HeaderText = "IDENTIFIANT";
                    dataGVPR.Columns[1].HeaderText = "NOM";
                    dataGVPR.Columns[2].HeaderText = "PRENOM";
                    dataGVPR.Columns[3].HeaderText = "MAIL";
                    dataGVPR.Columns[4].HeaderText = "TELEPHONE";


                    // mise à jour du dataGridView via le bindingSource rempli par le DataTable
                    dataGVPR.Refresh();
                    dataGVPR.Visible = true;
                    nb = 1;
                }




            }
        }

        private void BtnAjouter_Click(object sender, EventArgs e)
        {
            controleur.crud_personne('c', "");
            bS1.MoveLast();
            bS1.MoveFirst();
            TousLesUtilisateurs_Load(sender, e);
            dataGVPR.Refresh();
        }
    }
}
