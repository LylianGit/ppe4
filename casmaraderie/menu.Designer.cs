﻿namespace casmaraderie
{
    partial class menu
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.utilisateursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toutLesUtilisateursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connexionWIFIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hébergementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statistiquesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exporterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.utilisateursToolStripMenuItem,
            this.servicesToolStripMenuItem,
            this.statistiquesToolStripMenuItem,
            this.yOToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1257, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // utilisateursToolStripMenuItem
            // 
            this.utilisateursToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toutLesUtilisateursToolStripMenuItem});
            this.utilisateursToolStripMenuItem.Name = "utilisateursToolStripMenuItem";
            this.utilisateursToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.utilisateursToolStripMenuItem.Text = "Utilisateurs";
            this.utilisateursToolStripMenuItem.Click += new System.EventHandler(this.UtilisateursToolStripMenuItem_Click);
            // 
            // toutLesUtilisateursToolStripMenuItem
            // 
            this.toutLesUtilisateursToolStripMenuItem.Name = "toutLesUtilisateursToolStripMenuItem";
            this.toutLesUtilisateursToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.toutLesUtilisateursToolStripMenuItem.Text = "Tous les utilisateurs";
            this.toutLesUtilisateursToolStripMenuItem.Click += new System.EventHandler(this.ToutLesUtilisateursToolStripMenuItem_Click);
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.transportsToolStripMenuItem,
            this.connexionWIFIToolStripMenuItem,
            this.hébergementsToolStripMenuItem,
            this.coursToolStripMenuItem});
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.servicesToolStripMenuItem.Text = "Services";
            // 
            // transportsToolStripMenuItem
            // 
            this.transportsToolStripMenuItem.Name = "transportsToolStripMenuItem";
            this.transportsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.transportsToolStripMenuItem.Text = "Transports";
            this.transportsToolStripMenuItem.Click += new System.EventHandler(this.TransportsToolStripMenuItem_Click);
            // 
            // connexionWIFIToolStripMenuItem
            // 
            this.connexionWIFIToolStripMenuItem.Name = "connexionWIFIToolStripMenuItem";
            this.connexionWIFIToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.connexionWIFIToolStripMenuItem.Text = "ConnexionWIFI";
            this.connexionWIFIToolStripMenuItem.Click += new System.EventHandler(this.ConnexionWIFIToolStripMenuItem_Click);
            // 
            // hébergementsToolStripMenuItem
            // 
            this.hébergementsToolStripMenuItem.Name = "hébergementsToolStripMenuItem";
            this.hébergementsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.hébergementsToolStripMenuItem.Text = "Hébergements";
            this.hébergementsToolStripMenuItem.Click += new System.EventHandler(this.HébergementsToolStripMenuItem_Click);
            // 
            // coursToolStripMenuItem
            // 
            this.coursToolStripMenuItem.Name = "coursToolStripMenuItem";
            this.coursToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.coursToolStripMenuItem.Text = "Cours";
            this.coursToolStripMenuItem.Click += new System.EventHandler(this.CoursToolStripMenuItem_Click);
            // 
            // statistiquesToolStripMenuItem
            // 
            this.statistiquesToolStripMenuItem.Name = "statistiquesToolStripMenuItem";
            this.statistiquesToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.statistiquesToolStripMenuItem.Text = "Statistiques";
            this.statistiquesToolStripMenuItem.Click += new System.EventHandler(this.StatistiquesToolStripMenuItem_Click);
            // 
            // yOToolStripMenuItem
            // 
            this.yOToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importerToolStripMenuItem,
            this.exporterToolStripMenuItem});
            this.yOToolStripMenuItem.Name = "yOToolStripMenuItem";
            this.yOToolStripMenuItem.Size = new System.Drawing.Size(42, 20);
            this.yOToolStripMenuItem.Text = "BDD";
            // 
            // importerToolStripMenuItem
            // 
            this.importerToolStripMenuItem.Name = "importerToolStripMenuItem";
            this.importerToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.importerToolStripMenuItem.Text = "Importer";
            this.importerToolStripMenuItem.Click += new System.EventHandler(this.ImporterToolStripMenuItem_Click);
            // 
            // exporterToolStripMenuItem
            // 
            this.exporterToolStripMenuItem.Name = "exporterToolStripMenuItem";
            this.exporterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exporterToolStripMenuItem.Text = "Exporter";
            // 
            // menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1257, 625);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "menu";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Menu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem utilisateursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toutLesUtilisateursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statistiquesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem transportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connexionWIFIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hébergementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem coursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yOToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exporterToolStripMenuItem;
    }
}

