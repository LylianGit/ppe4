﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace casmaraderie
{
    public partial class menu : Form
    {
        public menu()
        {
            InitializeComponent();
        }

        private void StatistiquesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Statistiques FA = new Statistiques();
            FA.Show();
        }

        private void UtilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void ToutLesUtilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TousLesUtilisateurs FA = new TousLesUtilisateurs();
            FA.Show();
        }

        private void TransportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Transports FA = new Transports();
            FA.Show();
        }

        private void ConnexionWIFIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConnexionWIFI FA = new ConnexionWIFI();
            FA.Show();
        }

        private void HébergementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hébergements FA = new Hébergements();
            FA.Show();
        }

        private void CoursToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cours FA = new Cours();
            FA.Show();
        }

        private void ImporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controleur.Vmodele.import();
        }

        private void Menu_Load(object sender, EventArgs e)
        {
            
        }
    }
}
