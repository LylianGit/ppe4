﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using System.Collections;


namespace casmaraderie
{
    class modele
    {
        private MySqlConnection myConnection;
        private bool connopen = false;
        private bool errgrave = false;
        private bool chargement = false;

        private MySqlDataAdapter mySqlDataAdapterTP1 = new MySqlDataAdapter();
        private DataSet dataSetTP1 = new DataSet();
        private DataView dv_formation = new DataView(), dv_personne = new DataView(), dv_poste = new DataView();

        private bool errmaj = false;
        private char vaction, vtable;
        private ArrayList rapport = new ArrayList(); // pour gérer le rapport des erreurs de maj
        
        // collection de DataAdapter
        private List<MySqlDataAdapter> dA = new List<MySqlDataAdapter>();

        // collection de DataTable récupérant les données correspond au DA associé
        private List<DataTable> dT = new List<DataTable>();


        #region accesseurs
        public bool Connopen
        {
            get => connopen; set => connopen = value; }
        public bool Errgrave
        {
            get => errgrave; set => errgrave = value; }
        public bool Chargement
        {
            get => chargement; set => chargement = value; }
        public DataView Dv_formation
        {
            get => dv_formation; set => dv_formation = value; }
        public DataView Dv_personne
        {
            get => dv_personne; set => dv_personne = value; }
        public DataView Dv_poste
        {
            get => dv_poste; set => dv_poste = value; }
        public bool Errmaj
        {
            get => errmaj; set => errmaj = value; }
        public char Vaction
        {
            get => vaction; set => vaction = value; }
        public char Vaction1
        {
            get => vaction; set => vaction = value; }
        public ArrayList Rapport
        {
            get => rapport; set => rapport = value; }
        public List<DataTable> DT { get => dT; set => dT = value; }


        #endregion

        #region co deco bdd
        public void seconnecter()
        {
            string myConnectionString = "Database=jambon_beurre;Data Source=localhost;User Id=root;";
            myConnection = new MySqlConnection(myConnectionString);
            try // tentative
            {
                myConnection.Open();
                connopen = true;
            }
            catch (Exception err)// gestion des erreurs
            {
                MessageBox.Show("Erreur ouverture bdd : " + err, "PBS connection", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                connopen = false; errgrave = true;
            }
        }
        public void sedeconnecter()
        {
            if (!connopen)
                return;
            try
            {
                myConnection.Close();
                myConnection.Dispose();
                connopen = false;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur fermeture bdd : " + err, "PBS deconnection", MessageBoxButtons.OK,
                MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        #endregion

        #region import
        public void import()
        {
            if (!connopen) return;
            mySqlDataAdapterTP1.SelectCommand = new MySqlCommand("SELECT * FROM `wifi`; SELECT * FROM `transport`; SELECT * FROM `service`; SELECT * FROM `postule`; SELECT * FROM `possede`;" +
                " SELECT * FROM `personne` ; SELECT * FROM `options`; SELECT * FROM `offrant`; SELECT * FROM `lieu`; SELECT * FROM `hebergement`; SELECT * FROM `etudiant`; SELECT * FROM `etablissement`;" +
                " SELECT * FROM `cours`; SELECT * FROM `competence`; SELECT * FROM `avoir`; SELECT * FROM `appartient`; SELECT * FROM `acquereur`  ", myConnection);
            try
            {
                dataSetTP1.Clear();
                mySqlDataAdapterTP1.Fill(dataSetTP1);
                MySqlCommand vcommand = myConnection.CreateCommand();
                // gestion des clés auto_increment : ici exemple sur la table zpersonne : dataSetTP1.Tables[1]
                vcommand.CommandText = "SELECT AUTO_INCREMENT as last_id FROM INFORMATION_SCHEMA.TABLES WHERE table_name = 'personne'";
                UInt64 der_personne = (UInt64)vcommand.ExecuteScalar();
                dataSetTP1.Tables[1].Columns[0].AutoIncrement = true;
                dataSetTP1.Tables[1].Columns[0].AutoIncrementSeed = Convert.ToInt64(der_personne);
                dataSetTP1.Tables[1].Columns[0].AutoIncrementStep = 1;
                // remplissage des dataView à partir des tables du dataSet
                Dv_formation = dataSetTP1.Tables[0].DefaultView;
                Dv_personne = dataSetTP1.Tables[1].DefaultView;
                Dv_poste = dataSetTP1.Tables[2].DefaultView;
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataset : " + err, "PBS import",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }
        #endregion

        #region maj

        private void OnRowUpdated(object sender, MySqlRowUpdatedEventArgs args)
        // utile pour ajout,modif,supp
        {
            string msg = "";
            Int64 nb = 0;
            if (args.Status == UpdateStatus.ErrorsOccurred)
            {
                if (vaction == 'd' || vaction == 'u')
                {
                    MySqlCommand vcommand = myConnection.CreateCommand();
                    if (vtable == 'p') // ‘p’ pour table PERSONNE
                    {
                        vcommand.CommandText = "SELECT COUNT(*) FROM personne WHERE IdPer = '" +
                        args.Row[0, DataRowVersion.Original] + "'";
                    }
                    nb = (Int64)vcommand.ExecuteScalar();
                    // on veut savoir si la zpersonne existe encore dans la base
                }
                if (vaction == 'd')
                {
                    if (nb == 1) // pour delete si l'enr a été supprimé on n'affiche pas l'erreur
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de personnes : " + args.Row[0, DataRowVersion.Original] + "impossible delete car enr modifié dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                }
                if (vaction == 'u')
                {
                    if (nb == 1)
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de personne: " + args.Row[0, DataRowVersion.Original] + "impossible MAJ car enr modifié dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                    else
                    {
                        if (vtable == 'p')
                        {
                            msg = "pour le numéro de personne : " + args.Row[0, DataRowVersion.Original] + "impossible MAJ car enr supprimé dans la base";
                        }
                        rapport.Add(msg);
                        errmaj = true;
                    }
                }
                if (vaction == 'c')
                {
                    if (vtable == 'p')
                    {
                        msg = "pour le numéro de personne : " + args.Row[0, DataRowVersion.Current] + "impossible ADD car erreur données";
                    }
                    rapport.Add(msg);
                    errmaj = true;
                }
            }
        }
        #endregion

        #region charger
        /// <summary>
        /// Modele() : constructeur permettant l'ajout des DataAdpater et DataTable nécessaires (4 nécessaires pour l'existant actuel)
        /// indice 0 : récupération des noms des tables
        /// indice 1 : Table Etudiant
        /// indice 2 :
        /// indice 3 : 
        /// </summary>
        public modele()
        {

            for (int i = 0; i < 2; i++)
            {
                dA.Add(new MySqlDataAdapter());
                dT.Add(new DataTable());
            }
        }

        /// <summary>
        /// méthode générique privée pour charger le résultat d'une requête dans un dataTable via un dataAdapter
        /// Méthode appelée par charger_donnees(string table)
        /// </summary>
        /// <param name="requete">requete à charger</param>
        /// <param name="DT">dataTable</param>
        /// <param name="DA">dataAdapter</param>
        private void charger(string requete, DataTable DT, MySqlDataAdapter DA)
        {
            DA.SelectCommand = new MySqlCommand(requete, myConnection);

            // pour spécifier les instructions de mise à jour (insert, delete, update)
            MySqlCommandBuilder CB1 = new MySqlCommandBuilder(DA);
            try
            {
                DT.Clear();
                DA.Fill(DT);
                chargement = true;
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataTable : " + err, "PBS table", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }
        
        /// <summary>
        /// charge dans un DT les données de la table passée en paramètre
        /// </summary>
        /// <param name="table">nom de la table à requêter</param>
        public void charger_donnees(string table)
        {
            chargement = false;
            if (!connopen) return;		// pour vérifier que la BD est bien ouverte

            if (table == "toutes")
            {
                charger("show tables;", dT[0], dA[0]);
            }
            if (table == "etudiant")
            {
                charger("select * from etudiant;", dT[1], dA[1]);
            }
        }
        #endregion

        #region ajout/modif/suppr
        public void add_personne()
        {
            vaction = 'c'; // on précise bien l’action, ici c pour create
            vtable = 'p';
            if (!connopen) return;
            //appel d'une méthode sur l'événement ajout d'un enr de la table
            mySqlDataAdapterTP1.RowUpdated += new MySqlRowUpdatedEventHandler(OnRowUpdated);
            mySqlDataAdapterTP1.InsertCommand = new MySqlCommand("insert into etudiant (NOMETU, PRENOMETU, MAILETU, TELETU) values(?nom, ?prenom, ?mail, ?tel)", myConnection);// notre commandbuilder ici ajout non fait si erreur de données
            //déclaration des variables utiles au commandbuilder
            // pas besoin de créer l’IdPersonne car en auto-increment
            mySqlDataAdapterTP1.InsertCommand.Parameters.Add("?nom", MySqlDbType.Text, 50, "NOMETU");
            mySqlDataAdapterTP1.InsertCommand.Parameters.Add("?prenom", MySqlDbType.Text, 50, "PRENOMETU");
            mySqlDataAdapterTP1.InsertCommand.Parameters.Add("?mail", MySqlDbType.Int16, 11, "MAILETU");
            mySqlDataAdapterTP1.InsertCommand.Parameters.Add("?tel", MySqlDbType.Int16, 11, "TELETU");
            //on continue même si erreur de MAJ
            mySqlDataAdapterTP1.ContinueUpdateOnError = true;
            //table concernée 1 = zpersonne
            DataTable table = dataSetTP1.Tables[1];
            //on ne s'occupe que des enregistrement ajoutés en local
            mySqlDataAdapterTP1.Update(table.Select(null, null, DataViewRowState.Added));
            //désassocie la méthode sur l'événement maj de la base
            mySqlDataAdapterTP1.RowUpdated -= new MySqlRowUpdatedEventHandler(OnRowUpdated);
        }

        public void maj_personne()
        {
            vaction = 'u'; // on précise bien l’action, ici c pour create
            vtable = 'p';
            if (!connopen) return;
            //appel d'une méthode sur l'événement ajout d'un enr de la table
            mySqlDataAdapterTP1.RowUpdated += new MySqlRowUpdatedEventHandler(OnRowUpdated);
            mySqlDataAdapterTP1.UpdateCommand = new MySqlCommand("update etudiant set NOMETU=?nom, PRENOMETU=?prenom, MAILETU=?mail, TELETU=?tel where IdPer=?num and dateA=?modif", myConnection);
            //déclaration des variables utiles au commandbuilder
            // pas besoin de créer l’IdPersonne car en auto-increment
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?nom", MySqlDbType.Text, 50, "nom");
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?prenom", MySqlDbType.Text, 50, "prenom");
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?IdFormation", MySqlDbType.Int16, 11, "IdFormation");
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?IdPoste", MySqlDbType.Int16, 11, "IdPoste");
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?num", MySqlDbType.Int16, 11, "IdPer");
            mySqlDataAdapterTP1.UpdateCommand.Parameters.Add("?modif", MySqlDbType.Timestamp, 20, "dateA");
            //on continue même si erreur de MAJ
            mySqlDataAdapterTP1.ContinueUpdateOnError = true;
            //table concernée 1 = zpersonne
            DataTable table = dataSetTP1.Tables[1];
            //on ne s'occupe que des enregistrement ajoutés en local
            mySqlDataAdapterTP1.Update(table.Select(null, null, DataViewRowState.ModifiedCurrent));
            //désassocie la méthode sur l'événement maj de la base
            mySqlDataAdapterTP1.RowUpdated -= new MySqlRowUpdatedEventHandler(OnRowUpdated);
        }

        public void del_personne()
        {
            vaction = 'd'; // on précise bien l’action, ici c pour create
            vtable = 'p';
            if (!connopen) return;
            //appel d'une méthode sur l'événement ajout d'un enr de la table
            mySqlDataAdapterTP1.RowUpdated += new MySqlRowUpdatedEventHandler(OnRowUpdated);
            mySqlDataAdapterTP1.DeleteCommand = new MySqlCommand("delete from personne where IdPer=?num and dateA=?modif;", myConnection);// force le delete même si maj dans la base
            //déclaration des variables utiles au commandbuilder
            // pas besoin de créer l’IdPersonne car en auto-increment
            mySqlDataAdapterTP1.DeleteCommand.Parameters.Add("?num", MySqlDbType.Int16, 11, "IdPer");
            mySqlDataAdapterTP1.DeleteCommand.Parameters.Add("?modif", MySqlDbType.Timestamp, 20, "dateA");
            //on continue même si erreur de MAJ
            mySqlDataAdapterTP1.ContinueUpdateOnError = true;
            //table concernée 1 = zpersonne
            DataTable table = dataSetTP1.Tables[1];
            //on ne s'occupe que des enregistrement ajoutés en local
            mySqlDataAdapterTP1.Update(table.Select(null, null, DataViewRowState.Deleted));
            //désassocie la méthode sur l'événement maj de la base
            mySqlDataAdapterTP1.RowUpdated -= new MySqlRowUpdatedEventHandler(OnRowUpdated);
        }
        #endregion

        #region export
        public void export()
        {
            if (!connopen) return;
            try
            {
                add_personne();
                maj_personne();
                del_personne();
            }
            catch (Exception err)
            {
                MessageBox.Show("Erreur chargement dataset : " + err, "PBS export",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
                errgrave = true;
            }
        }

        #endregion
    }

}
